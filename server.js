const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const env = require("dotenv");
const passport = require("passport");
const morgan = require("morgan");


env.config();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(passport.initialize());
require("./src/config/passport")(passport);

const prefix = "/workfhome";

app.use(prefix + "/auth", require("./src/routes/auth"));
app.use(prefix , require("./src/routes/brand"));
app.use(prefix , require("./src/routes/model"));
app.use(prefix , require("./src/routes/order"));

app.use((req, res, next) => {
  const error = new Error("404 Not Found!");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  const status = error.status || 500;
  return res.status(status).json({
    status: status,
    message: error.message
  });
});

var port = process.env.PORT || 3000;
app.listen(port);

console.log("app is running at http://localhost:" + port);
