exports.validationError = result => {
    let errors = "";
    if (result.error) {
      errors = result.error.details.map(item => {
        return { field: item.path[0], message: item.message };
      });
    }
    return { message: "Validation Error", error: errors };
  };
  