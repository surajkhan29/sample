const nodemailer = require("nodemailer");
exports.send = async (to, subject, message, next) => {
  try {
    var transport = nodemailer.createTransport({
      host: "smtp.mailtrap.io",
      port: 2525,
      auth: {
        user: "e6ddb8edd44b79",
        pass: "edbc49d4c6c7eb"
      }
    });

    const mailOptions = {
      from:'"Activation message" <satheeshtest@gmail.com>' ,
      to: to,
      subject: 'Please confirm account',
      generateTextFromHTML: true,
      html: message
    };

    let result = await transport.sendMail(mailOptions);
    // console.log(result);
    transport.close();
    return result;
  } catch (error) {
    next(error);
  }
};
