const sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const AED = db.define(
  "aeds",
  {
    brand_id: sequelize.INTEGER,
    model_id: sequelize.INTEGER,
    serial: sequelize.STRING,
    reg: sequelize.STRING,
    purchase_date: sequelize.DATEONLY,
    street: sequelize.STRING,
    country: sequelize.STRING,
    zipcode: sequelize.STRING,
    status: { type: sequelize.STRING, defaultValue: 1 }
  },
  {
    paranoid: true,
    underscored: true
  }
);

AED.sync();

const BRAND = db.define(
  "brands",
  {
    name: sequelize.STRING
    
  },
  {
    paranoid: true,
    underscored: true
  }
);

BRAND.sync();

const MODEL = db.define(
  "models",
  {
    name: sequelize.STRING,
    brand_id: sequelize.STRING
    
  },
  {
    paranoid: true,
    underscored: true
  }
);

MODEL.sync();


const AEDSchema = Joi.object({
    brand_id: Joi.number().required(),
    model_id: Joi.number().required(),
    serial: Joi.string().required(),
    reg: Joi.string().required(),
    street: Joi.string().required(),
    country: Joi.string().required(),
    zipcode: Joi.string().required()
    
    
  });
  const BRANDSchema = Joi.object({
    name: Joi.string().required()

  });
  const MODELSchema = Joi.object({
    name: Joi.string().required(),
    brand_id: Joi.string().required()
  });

// TESTING.belongsToMany(CHEF), { foreignKey: "chef_id" };
// CHEF.belongsToMany(RECIPE, { through: 'TESTING' });
// COMMENT.belongsTo(RECIPE, { foreignKey: "recipe_id" });
// RECIPE.hasOne(COMMENT, {foreignKey: "recipe_id"});

// RECIPE.hasMany(COMMENT, { foreignKey: "recipe_id", as: "comments" });
// RECIPE.hasOne(COMMENT, { foreignKey: "recipe_id" });
// COMMENT.belongsTo(RECIPE, { foreignKey: "recipe_id" });

// RECIPE.belongsToMany(CHEF, {
//   foreignKey: "recipe_id",
//   through: /*table name*/ "tests"
// }); //if you want to st module name set thtough: TESTING
// CHEF.belongsToMany(RECIPE, { foreignKey: "chef_id", through: "tests" });
BRAND.hasMany(AED,{ foreignKey: "brand_id", as: "aeds" });
BRAND.hasMany(MODEL,{ foreignKey: "brand_id", as: "models" });
MODEL.hasMany(AED,{ foreignKey: "model_id", as: "aeds" });
module.exports = { BRAND, MODEL, AED, MODELSchema, BRANDSchema, AEDSchema };
