const Sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const User = db.define(
  "users",
  {
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    firstname: Sequelize.STRING,
    lastname: Sequelize.STRING,
    is_status: { type: Sequelize.INTEGER, defaultValue: 0 },
    remember_token: { type: Sequelize.STRING, allowNull: true },
    last_login: { type: Sequelize.DATE, allowNull: true }
    
  },
  {
    paranoid: true,
    underscored: true,
    timestamps: true
  }
);

User.sync();

//Validation Schema
const ValSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
  password: Joi.string()
    .pattern(
      new RegExp(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/
      )
    )
    .required(),
  firstname: Joi.string().required(),
  lastname: Joi.string().required(),
  last_login: Joi.date().allow(null)
 
 
 
});

const LoginSchema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required()
});

const restSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
  newpassword: Joi.string()
    .pattern(
      new RegExp(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/
      )
    ).required(),
  token: Joi.string().required()
});
const tokenSchema = Joi.object({
token: Joi.number().required()
});
module.exports = { User, ValSchema, LoginSchema, restSchema, tokenSchema };
