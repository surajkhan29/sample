const router = require("express").Router();
const { User, ValSchema, LoginSchema, restSchema } = require("../models/User");
const {  AED, AEDSchema } = require("../models/AED");
const { validationError } = require("../helpers/common");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Joi = require("@hapi/joi");
const mail = require("../config/mail");
var uuid = require("uuid");
const Sequelize = require("sequelize");
const randomize = require('randomatic');

router.post("/order", async (req, res, next) => {
    try {
      const validateResult = AEDSchema.validate(req.body, {
        abortEarly: false
      });
      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));
       
     let order = await AED.create(req.body);
     order.update ({ purchase_date: Date.now() });
      return res.status(201).json("AED Created");
    } catch (error) {
      console.log(error);
      next(error);
    }
  });
  
  router.get('/order/:id',async (req, res, next) => {
      try{
      let brand_id = await AED.findAll({ where: { id: req.params.id } });
      
          return res.status(201).json(brand_id);
  
      }
      catch (error) {
      console.log(error);
      next(error);
    }
  });


  router.patch("/order/id/:id/restore", async (req, res, next) => {
    try {
        const validateResult = AEDSchema.validate(req.body, {
            abortEarly: false
          });
          if (validateResult.error)
            return res.status(422).json(validationError(validateResult));
      let brand = await AED.update(
        { name: req.body.name,
           },
        { where: { id: req.params.id } }
      );
  
      return res.status(200).json(brand);
    } catch (error) {
      //   }
      console.log(error);
      next(error);
    }
  });

router.delete("/order/id/:id", async (req, res, next) => {
  try {
    let delbrand = await AED.destroy({ where: { id: req.params.id } });
    return res.status(204).json(delbrand);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});
module.exports = router;



  
