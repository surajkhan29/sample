const router = require("express").Router();
const {  MODEL, MODELSchema } = require("../models/AED");
const { validationError } = require("../helpers/common");

router.post("/model", async (req, res, next) => {
    try {  
      const validateResult = MODELSchema.validate(req.body, {
        abortEarly: false
      });
      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));
  
       await MODEL.create(req.body);
      return res.status(201).json("Model Created");
    } catch (error) {
      console.log(error);
      next(error);
    }
  });
  
  router.get('/brand/:id/models',async (req, res, next) => {
      try{
      let brand_id = await MODEL.findAll({ where: { brand_id: req.params.id } });
      
          return res.status(201).json(brand_id);
  
      }
      catch (error) {
      console.log(error);
      next(error);
    }
  });

  
  router.patch("/model/id/:id/restore", async (req, res, next) => {
    try {
       
      let model = await MODEL.update(
        { name: req.body.name,
            brand_id: req.body.brand_id},
        { where: { id: req.params.id } }
      );
  
      return res.status(200).json(model);
    } catch (error) {
      //   }
      console.log(error);
      next(error);
    }
  });

router.delete("/model/id/:id", async (req, res, next) => {
  try {
    let delmod = await MODEL.destroy({ where: { id: req.params.id } });
    return res.status(204).json(delmod);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});

module.exports = router;