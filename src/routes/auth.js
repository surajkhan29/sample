const router = require("express").Router();
const { User, ValSchema, LoginSchema, restSchema, tokenSchema } = require("../models/User");
const { validationError } = require("../helpers/common");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const mail = require("../config/mail");
var uuid = require("uuid");
const Sequelize = require("sequelize");
const randomize = require('randomatic');


router.post(
  "/login",

  async (req, res, next) => {
    try {
      const validateResult = LoginSchema.validate(req.body, {
        abortEarly: false
      });

      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));
      let usernameOrEmail = req.body.email;
      let password = req.body.password;

      let user = await User.findOne({
        where: {
          [Sequelize.Op.or]: {
            firstname: usernameOrEmail,
            email: usernameOrEmail
          },
          is_status: 1
        }
      });

      if (user) {
        let result = await bcrypt.compare(password, user.password);
        if (result) {
          const token = jwt.sign(
            { firstname: usernameOrEmail, password: password },
            process.env.JWT_SECRET,
            { expiresIn: "36500d" }
          );
          user.update({ last_login: Date.now(), remember_token: token });

          res.status(201).json({
            success: 1,
            data: {
              access_token: "Bearer " + token,
              user: user
            },
            message: "Login Success",
            user_id: user.id
          });
        } else {
          res.status(404).json({
            success: 0,
            error: 404,
            data: {},
            message: "Username or Password is not found in our records"
          });
        }
      } else {
        res.status(400).json({
          success: 0,
          error: 400,
          data: {},
          message: "User Is Not Active Please Active Now"
        });
      }
    } catch (err) {
      next(err);
    }
  }
);
//dfvd




router.patch("/token", async (req, res, next) => {
  try {
    const validateResult = tokenSchema.validate(req.body, {
      abortEarly: false
    });

    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    token = req.body.token;
    let update_user = await User.update(
      {
        is_status: 1,
        remember_token: null
      },
      { where: { remember_token: token } }
     
    );
   
    if(update_user == 0)
{
    return res.status(400).json("Code Is Not Match");
}
     else{return res.status(200).json("User Is Active Now");}

  } catch (error) {
    console.log(error);
    next(error);
  }
});



router.patch("/password/reset", async (req, res, next) => {
  try {
    const validateResult = restSchema.validate(req.body, {
      abortEarly: false
    });

    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let exists = await User.findOne({ where: { email: req.body.email } });
    if(exists.remember_token == req.body.token)
    {
      req.body.newpassword = bcrypt.hashSync(req.body.newpassword, 10);
      token = req.body.token;
      let update_user = await User.update(
        {
          password: req.body.newpassword
        },
        { where: { remember_token: token } }
      );
      await User.update(
        {
          remember_token: null
        },
        { where: { email: req.body.email } }
      );
     res.status(200).json("Password rest success");
    }
    
   return res.status(400).json("token is inncorrect");
  } catch (error) {
    console.log(error);
    next(error);
  }
});



router.post("/forgot-password", async (req, res, next) => {
  try {
   
    let exists = await User.findOne({ where: { email: req.body.email } });
    if (exists != null && exists.is_status == 1)
    {
    const token = uuid.v1();
    const url = token;
    req.body.remember_token = token;
    let update_user = await User.update(
      {
        remember_token: token
      },
      { where: { email: req.body.email } }
    );
    let sentMail = await mail.send(

      req.body.email,
      "Activate your account",
      `Your rest password request is successs. Your verification code is below: <br><b>"${url}"<b></br>`,
      next
    );
    
    return res.status(200).json("Verifiaction Code Sent To Email");
    }
    return res.status(400).json("User Not Active or Email Not Valid");
  } catch (error) {
    console.log(error);
    next(error);
  }
});




router.post("/register", async (req, res, next) => {
  try {
    const validateResult = ValSchema.validate(req.body, {
      abortEarly: false
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let exists = await User.findOne({ where: { email: req.body.email } });
    if (exists != null)
      return res.status(400).json("Email is already registered");

    let uname = await User.findOne({ where: { firstname: req.body.firstname } });
    if (uname != null) return res.status(400).json("user name already exits");

    req.body.password = bcrypt.hashSync(req.body.password, 10);
    const token = randomize('0000');
    const url = token;
    req.body.remember_token = token;
    let sentMail = await mail.send(

      req.body.email,
      "Activate your account",
      `Thank you for registration. Your verification code is below: <br><b>"${url}"<b></br>`,
      next
    );

    let new_user = await User.create(req.body);

    return res.status(201).json(new_user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});



module.exports = router;