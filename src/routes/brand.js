const router = require("express").Router();
const {  BRAND, BRANDSchema } = require("../models/AED");
const { validationError } = require("../helpers/common");


router.post("/brand", async (req, res, next) => {
    try {
      const validateResult = BRANDSchema.validate(req.body, {
        abortEarly: false
      });
      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));
  
     await BRAND.create(req.body);
      return res.status(201).json("Brand Created");
    } catch (error) {
      console.log(error);
      next(error);
    }
  });
  
  router.get('/brand/:id',async (req, res, next) => {
      try{
      let brand_id = await BRAND.findAll({ where: { id: req.params.id } });
      
          return res.status(201).json(brand_id);
  
      }
      catch (error) {
      console.log(error);
      next(error);
    }
  });


  router.patch("/brand/id/:id/restore", async (req, res, next) => {
    try {
        const validateResult = BRANDSchema.validate(req.body, {
            abortEarly: false
          });
          if (validateResult.error)
            return res.status(422).json(validationError(validateResult));
      let brand = await BRAND.update(
        { name: req.body.name,
           },
        { where: { id: req.params.id } }
      );
  
      return res.status(200).json(brand);
    } catch (error) {
      //   }
      console.log(error);
      next(error);
    }
  });

router.delete("/brand/id/:id", async (req, res, next) => {
  try {
    let delbrand = await BRAND.destroy({ where: { id: req.params.id } });
    return res.status(204).json(delbrand);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});
module.exports = router;